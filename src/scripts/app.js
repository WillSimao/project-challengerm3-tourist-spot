document.addEventListener('DOMContentLoaded', function () {
    const list = [];


    const form = document.querySelector('.container-form');
    const imageInput = document.querySelector('.image-input-file');
    const titleInput = document.querySelector('.input-add-title');
    const descriptionInput = document.querySelector('.input-add-description');
    const newSpot = document.querySelector('.card-new');
    const preview = document.querySelector('.img-file');
    const imageInputSpan = document.querySelector('.image-input-span');
    const imageInputLabel = document.querySelector('.image-input-label');




    form.addEventListener('submit', addNewSpot);
    imageInput.addEventListener('change', readImg);

    function addNewSpot(event) {
        event.preventDefault();

        const spotImage = sessionStorage.getItem('imageURL');
        const spotTitle = event.target['input-add-title'].value;
        const spotDescription = event.target['input-add-description'].value;


        if (spotTitle != '' && spotDescription != '') {
            const spot = {
                image: spotImage,
                title: spotTitle,
                description: spotDescription,
            };

            list.push(spot);

            renderSpot();
            resetInputs();
        }
    }

    function readImg () {
        imageInputSpan.style = 'display: none;'
        imageInputLabel.style = 'border: none;'



        const reader = new FileReader();
        reader.onload = function () {
            sessionStorage.setItem('imageURL', reader.result);
            preview.src = reader.result;
            preview.style.display = 'block';
        }
        reader.readAsDataURL(imageInput.files[0]);

    }


    function renderSpot() {
        let spotStructure = '';

        list.forEach(function (spot) {
            spotStructure += `
                <div class='result-card'>
                    <img class='result-card-image' src='${spot.image}' alt='' />
                    <div class='result-card-text'>
                        <h2 class='result-card-title'>${spot.title}</h2>
                        <p class="result-card-description">${spot.description}</p>
                    </div>
                </div>
                `;

        });

        imageInputSpan.style = 'display:;'
        imageInputLabel.style = 'border:;'
        preview.style = 'display:;'
        newSpot.innerHTML = spotStructure;
    }


    function resetInputs() {
        preview.src = '';
        imageInput.value = '';
        titleInput.value = '';
        descriptionInput.value = '';

    }
})
